FROM node:14.15.4-alpine

WORKDIR /myapp

#RUN npm install -g pm2

COPY package*.json ./

RUN npm install

COPY . .

#CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]
CMD [ "npm","start" ]

EXPOSE 3000