use myfile;
create table details(
    id int auto_increment,
    createdAt timestamp default CURRENT_TIMESTAMP,
    updateAt timestamp default CURRENT_TIMESTAMP,
    fullName varchar(255),
    email varchar(255),
    phone varchar(255),
    UNIQUE (email),
    primary key (id)
);
create table accounts(
    id int auto_increment,
    createdAt timestamp default CURRENT_TIMESTAMP,
    updateAt timestamp default CURRENT_TIMESTAMP,
    username varchar(255),
    pass varchar(255),
    idUser int,
    primary key (id),
    UNIQUE (username),
    Unique (idUser),
    constraint fk_de_ac foreign key (idUser) references details(id)
);
create table files(
    id int auto_increment,
    createdAt timestamp default CURRENT_TIMESTAMP,
    updateAt timestamp default CURRENT_TIMESTAMP,
    bucketName varchar(255),
    objectName varchar(1024),
    shareLocation varchar(2048),
    primary key (id),
    constraint fk_ac_fi foreign key (bucketName) references accounts(username)
);