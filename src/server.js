const express = require('express')
const morgan = require('morgan')
const route = require('./app/app.route')
const http = require('http')
const app = express()
const port = 3000
const server = http.createServer(app)
const doc = require('./config/swagger/config')

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({
  extended: true
}));

//morgan
app.use(morgan('dev'))

route(app)

//doc
const expressSwagger = require('express-swagger-generator')(app);

expressSwagger(doc)

module.exports = server
server.listen(port, () => {
  console.log(`server listening on http://localhost:${port}`)
})