const mysql = require('mysql')
const config = require("../../config/db/driver")
class Account {
    constructor(account) {
        this.username = account.username
        this.pass = account.password
    }
}
Account.findOne = (params) => {
    return new Promise((resolve, reject) => {
        let sqlText = "SELECT * FROM `accounts`"
        console.log(Object.keys(params).length)
        if (Object.keys(params).length > 0) {
            let flag = 1;
            sqlText += ' WHERE '
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    if (flag > 1) {
                        sqlText += " AND "
                    }
                    sqlText += i + " = " + (typeof params[i] == 'string' ? `'${params[i]}'` : params[i])
                    flag++
                }
            }
        }
        sqlText += " LIMIT 1"   
        var connect = mysql.createConnection(config)
        connect.query(sqlText, (err, result) => {
            if (err) reject(err)
            if (result) {
                resolve(result[0])
            } else {
                resolve({})
            }
        })
        connect.end()
    })

}
Account.create = (account) => {
    return new Promise((resolve, reject) => {
        var connect = mysql.createConnection(config)
        connect.query("INSERT INTO accounts SET ?", account, (err, result) => {
            if (err) reject(err)
            resolve(result.insertId)
        })
        connect.end()
    })
}

module.exports = Account