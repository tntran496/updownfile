const Account = require('./account.model')
const Detail = require('../details/details.model')
const aut = require('../middleware/auth')
const express = require('express')
const route = express.Router()

/**
 * @typedef Account
 * @property {string} username
 * @property {string} pass
 */
 /**
 * @typedef Error
 * @property {string} message
 */
/**
 * @typedef Response
 * @property {string} message
 */
/**
 * This function comment is parsed by doctrine
 * @route POST /api/v1/account/login
 * @group account - Operations about account 
 * @param {Account.model} account.body.required - the new account
 * @returns {Response.model} 200 - An token to authenication
 * @returns {Error.model}  Error - Unexpected error
 */
route.post('/login',async (req,res,next) => {
    try {
        const {body} = req
        const result = await Account.findOne(body)
        if(result){
            res.status(200).send({message : `Bearer ${aut.generateJWT({username : result.id})}`})
        }else{
            res.status(404).send({message : "Login Fail"})
        }
    } catch (error) {
        next(error)
    }
    
})

/**
 * @typedef Payload
 * @property {string} fullName
 * @property {string} email
 * @property {string} phone
 * @property {string} username
 * @property {string} pass
 */
/**
 * This function comment is parsed by doctrine
 * @route POST /api/v1/account/register
 * @group account - Operations about account 
 * @param {Payload.model} payload.body.required - regiter new user and account
 * @returns {Response.model} 200 - An token to authenication
 * @returns {Error.model}  Error - Unexpected error
 */
route.post('/register',async (req,res,next) => {
    try {
        const {body} = req
        const detail = {
            fullName : body.fullName,
            email: body.email,
            phone: body.phone
        }
        const idDetail = await Detail.create(detail)
        
        const result = await Account.create({username : body.username,pass : body.pass,idUser : idDetail})
        if(result){
            res.status(200).send({message : "Create Success"})
        }else{
            res.status(500).send({message : "Create Fail"})
        }
    } catch (error) {
        next(error)
    }
})
module.exports = route