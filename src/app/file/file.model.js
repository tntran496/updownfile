const mysql = require('mysql')
const config = require("../../config/db/driver")
class Files {
    constructor(file) {
        this.bucketName = file.bucketName
        this.objectName = file.objectName
    }
}

Files.create = (file) => {
    return new Promise((resolve, reject) => {
        var connect = mysql.createConnection(config)
        connect.query("INSERT INTO files SET bucketName = ? , objectname = ?", [file.bucketName, file.objectName], (err, results) => {
            if (err) reject(err)
            resolve(results.insertId)
        })
        connect.end()
    })
}

Files.find = (params) => {
    return new Promise((resolve, reject) => {
        let sqlText = "SELECT * FROM `files`"
        if (Object.keys(params).length > 0) {
            let flag = 1;
            sqlText += ' WHERE '
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    if (flag > 1) {
                        sqlText += " AND "
                    }
                    sqlText += i + " = " + (typeof params[i] == 'string' ? `'${params[i]}'` : params[i])
                    flag++
                }
            }
        }
        var connect = mysql.createConnection(config)
        connect.query(sqlText, (err, result) => {
            if (err) reject(err)
            resolve(result)
        })
        connect.end()
    })
}

Files.findOne = (params) => {
    return new Promise((resolve, reject) => {
        let sqlText = "SELECT * FROM `files`"
        console.log(Object.keys(params).length)
        if (Object.keys(params).length > 0) {
            let flag = 1;
            sqlText += ' WHERE '
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    if (flag > 1) {
                        sqlText += " AND "
                    }
                    sqlText += i + " = " + (typeof params[i] == 'string' ? `'${params[i]}'` : params[i])
                    flag++
                }
            }
        }
        sqlText += " LIMIT 1"
        console.log(sqlText)
        var connect = mysql.createConnection(config)
        connect.query(sqlText, (err, result) => {
            if (err) reject(err)
            if (result) {
                resolve(result[0])
            } else {
                resolve({})
            }
        })
        connect.end()
    })
}
module.exports = Files