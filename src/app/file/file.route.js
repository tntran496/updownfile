const FileSaver = require('file-saver')
const Files = require("./file.model")
const Fs = require('fs')
const Minio = require('minio')
const express = require('express')
const multer = require('multer')
const aut = require('../middleware/auth')
const min_env = require("../../config/minio/config")
const {
    ifError
} = require("assert")
const upload = multer({
    dest: 'uploads/'
})
const route = express.Router()

 /**
 * @typedef Error
 * @property {string} message
 */
/**
 * @typedef Response
 * @property {string} message
 */
/**
 * @typedef URL
 * @property {string} presignedUrl
 */
/**
 * This function comment is parsed by doctrine
 * @route GET /api/v1/upload/:objectName
 * @group file - Operations about file
 * @returns {URL.model} 200 - message if dowload success
 * @returns {Response.model}  default - Unexpected error
 */
route.get('/:objectName',aut.auth,async (req,res,next) => {
   try {
        const {user,params} = req
        const minioClient = new Minio.Client(min_env)
        const url = await minioClient.presignedGetObject(user.username, params.objectName,24*60*60)
        if(url){
            res.status(200).send({presignedUrl : url})
        }else{
            res.status(500).send({message : 'Download error'})
        }
   } catch (error) {
       next(error)
   }
    
})
 /**
 * @typedef File
 * @property {int} id
 * @property {string} objectName
 * @property {string} bucketName
 * @property {string} presignedUrl
 */
/**
 * This function comment is parsed by doctrine
 * @route GET /api/v1/upload
 * @group file - Operations about file
 * @headers {string} token.require
 * @returns {File.model} 200 - An array of file info
 * @returns {Response.model} - Unexpected error
 */
route.get('/',aut.auth,async (req,res,next) =>{
    try{
        const {user} = req
        const bucketName =  user.username
        const minioClient = new Minio.Client(min_env)
        let listResult = []
        let results = await Files.find({bucketName : bucketName})
        for(const i in results)
        {
            results[i].presignedUrl = await minioClient.presignedGetObject(bucketName, results[i].objectName, 24*60*60)
            listResult.push(results[i])
        }
        res.status(200).send(listResult)
    }catch(err){
        next(err)
    }
})
/**
 * This function comment is parsed by doctrine
 * @route POST /api/v1/upload
 * @group file - Operations about file 
 * @param {file} file.body.required - upload new file
 * @returns {File.model} 200 - an object of file
 * @returns {Response.model} default - Unexpected error
 */
route.post('/',aut.auth, upload.single('img'), async (req, res, next) => {
    try {
        const { user,file, params } = req
        const fileStream = Fs.createReadStream(file.path)
        const checkExistNameData  = await Files.findOne({objectName : file.originalname})
        if(checkExistNameData){
            res.status(400).send({error : "File exist"}) 
            return
        }
        
        const newFile = {
            bucketName : user.username,
            objectName : file.originalname,
        }
        const minioClient = new Minio.Client(min_env);
        console.log(minioClient)
        
        let exist = await minioClient.bucketExists(newFile.bucketName)
        console.log(exist)
        if (!exist) {
            console.log("Debug")
            await minioClient.makeBucket(newFile.bucketName, min_env.location)
           
        }
        await minioClient.putObject(newFile.bucketName, newFile.objectName, fileStream, file.size, file.mimetype)

        let id = await Files.create(newFile)
        let result = await Files.findOne({id : id})
        console.log(result)
        result.presignedUrl = await minioClient.presignedGetObject(result.bucketName, result.objectName, 24*60*60)

        res.send(result)

    } catch (error) {
        console.log(error)
        next(error)
    }
})
module.exports = route