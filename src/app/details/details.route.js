const Detail = require('./details.model')
const express = require('express')
const route = express.Router()

/**
 * @typedef Model
 * @property {string} id
 * @property {string} fullName
 * @property {string} email
 * @property {string} phone
 */
/**
 * This function comment is parsed by doctrine
 * @route GET /api/v1/user/:id
 * @group details - Operations about user
 * @param {int} id.params.required - id user.
 * @returns {Model.model} 200 - An array of user info
 * @returns {message : "User not found!"}  default - Unexpected error
 */
route.get('/:id',async (req,res,next)=>{
    try {
        const {params} = req
        const result = await Detail.findOne({id : params.id})
        if(result){
            res.status(200).send(result)
        }else{
            res.status(404).send({message : "User not found!"})
        }
    } catch (e) {
        next(e)
    }
})

module.exports = route