const mysql = require('mysql')
const config = require("../../config/db/driver")
class Detail {
    constructor(detail) {
        this.fullName = detail.fullName
        this.email = detail.email
        this.phone = detail.phone
    }
}
Detail.find = (params) => {
    return new Promise((resolve, reject) => {
        let sqlText = "SELECT * FROM `details`"
        if (Object.keys(params).length > 0) {
            let flag = 1;
            sqlText += ' WHERE '
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    if (flag > 1) {
                        sqlText += " AND "
                    }
                    sqlText += i + " = " + (typeof params[i] == 'string' ? `'${params[i]}'` : params[i])
                    flag++
                }
            }
        }
        var connect = mysql.createConnection(config)
        connect.query(sqlText, (err, result) => {
            if (err) reject(err)
            resolve(result)
        })
        connect.end()
        // connect.destroy();
    })
}
Detail.findOne = (params) => {
    return new Promise((resolve, reject) => {
        let sqlText = "SELECT * FROM `details`"
        console.log(Object.keys(params).length)
        if (Object.keys(params).length > 0) {
            let flag = 1;
            sqlText += ' WHERE '
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    if (flag > 1) {
                        sqlText += " AND "
                    }
                    sqlText += i + " = " + (typeof params[i] == 'string' ? `'${params[i]}'` : params[i])
                    flag++
                }
            }
        }
        sqlText += " LIMIT 1"
        var connect = mysql.createConnection(config)
        connect.query(sqlText, (err, result) => {
            if (err) reject(err)
            if (result) {
                resolve(result[0])
            } else {
                resolve({})
            }
        })
        connect.end()
    })
}
Detail.create = (detail) => {
    return new Promise((resolve, reject) => {
        var connect = mysql.createConnection(config)
        connect.query('INSERT INTO details SET ?', detail, (err, result) => {
            if (err) reject(err)
            resolve(result.insertId)
        })
        connect.end()
    })
}
module.exports = Detail