const jwt = require('jsonwebtoken')
const Account = require('../account/account.model')

const myKey = 'UpDownFileMinio'

const auth = async(req, res, next) => {
    try {
        let token = req.header('Authorization').split(' ')
        console.log(token[2])
        const data = jwt.verify(token[2],myKey)
        console.log(data)
        const user = await Account.findOne(data.username)
        console.log(user)
        if (!user) {
            throw new Error()
        }
        req.user = user
        req.token = token
        next()
    } catch (error) {
        res.status(401).send({ error: 'Not authorized to access this resource' })
    }
}

function generateJWT(acccount){
    return jwt.sign(acccount,myKey)
}

module.exports = {auth,generateJWT}