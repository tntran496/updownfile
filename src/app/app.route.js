const fileRoute = require('./file/file.route')
const accountRoute = require('./account/account.route')
const userRoute = require('./details/details.route')
function route(app){
    app.use('/api/v1/upload',fileRoute)
    app.use('/api/v1/account',accountRoute)
    app.use('/api/v1/user',userRoute)
}
module.exports = route