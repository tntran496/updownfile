const min_env = {
     location : "us-east-1",
     endPoint : process.env.MIN_HOST||'localhost',
     port: 9000,
     accessKey : "minio",
     secretKey : "minio123",
     useSSL: false
}
module.exports = min_env