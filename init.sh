#!/bin/bash
docker-compose down
rm -rf .docker
rm -rf uploads
docker image rmi registry.gitlab.com/tntran496/updownfile:v1
docker build -t registry.gitlab.com/tntran496/updownfile:v1 .
docker-compose up --force-recreate