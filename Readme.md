# Upload And Dowload File Api
### Clone project
```
git clone https://gitlab.com/tntran496/updownfile.git
```
### Go top roject and Start with docker-compose in project (need install docker and docker-compose)
```bat
    cd /updownfile
    docker-compose up
```

### Server listen on
```
    http://localhost:3000
```
### Documentation api
```
    http://localhost:3000/api-docs
```
